/*
 * CSc103 Project 3: unix utilities
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 * Saddique Shafi (Tutor)
 * Bhavesh Shah (Tutor)
 * Marty
 * Audite T. (Classmate)
 * Stefan Tan (Classmate)
 * Abel (Classmate)
 * Ai Hua (Classmate)
 * geeksforgeeks.com
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: 19
 */

#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <vector>
using std::vector;
#include <algorithm>
using std::sort;
#include <string.h> // for c-string functions.
#include <getopt.h> // to parse long arguments.
#include <map>
using std::map;

static const char* usage =
"Usage: %s [OPTIONS] SET1 [SET2]\n"
"Limited clone of tr.  Supported options:\n\n"
"   -c,--complement     Use the complement of SET1.\n"
"   -d,--delete         Delete characters in SET1 rather than translate.\n"
"   --help          show this message and exit.\n";

void escape(string& s) {
	/* NOTE: the normal tr command seems to handle invalid escape
	 * sequences by simply removing the backslash (silently) and
	 * continuing with the translation as if it never appeared. */
	/* TODO: write me... */

	for(int i=0; i<=s.length(); i++) {
		if (s[i] == '\\'){
			if(s[i + 1] == '\\')
				s[i+1] = '\\';
			else if (s[i+1] == 'n')
				s[i+1] = '\n';
			else if (s[i + 1] == 't')
				s[i+1] = '\t';
			s.erase(s.begin()+i);
		}
	}

}

int main(int argc, char *argv[])
{
	// define long options
	static int comp=0, del=0;
	static struct option long_opts[] = {
		{"complement",      no_argument,   0, 'c'},
		{"delete",          no_argument,   0, 'd'},
		{"help",            no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "cdh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				comp = 1;
				break;
			case 'd':
				del = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}
	if (del) {
		if (optind != argc-1) {
			fprintf(stderr, "wrong number of arguments.\n");
			return 1;
		}
	} else if (optind != argc-2) {
		fprintf(stderr,
				"Exactly two strings must be given when translating.\n");
		return 1;
	}
	string s1 = argv[optind++];
	string s2 = (optind < argc)?argv[optind]:"";
	/* process any escape characters: */
	escape(s1);
	escape(s2);

	/* TODO: finish this... */
	map<char, char>m;
//	for(int i=0; i < s1.length(); i++){
//		m[s1[i]] = s2[i];
//	}
	if(s1.length() > s2.length()){ //if set1 bigger than set2
		for(int i=0; i < s2.length(); i++){
			m[s1[i]] = s2[i];
		}
		for(int i = s1.length()-s2.length(); i<s1.length(); i++){ //loop again
			m[s1[i]] = s2.back();
		}
	}
	else if(s1.length() == s2.length()){ //set 1 equal set2
		for(int i=0; i < s1.length(); i++){
			m[s1[i]] = s2[i];
		}
	}

	string input;
	string output;
	char in;

	while (fread(&in,1,1,stdin)){
		input.push_back(in); //reading input from command line
	}
	map<char,char>::iterator itr;
 	for(int i=0; i<input.length(); i++){ //using itr to go through the input
  		itr = m.find(input[i]);
		if(comp == 0 && del == 0) { //created b/c otherwise printint twice and incorrectly
			if(itr != m.end()){ //if input not in SET1, go to the end of the map
					output.push_back(itr->second); //committing changes into output
			}else{
				output.push_back(input[i]);
			}
		}
 	}

	//-d
	if(comp == 0 && del == 1){
		for(int i = 0; i < input.length(); i++){
			if(s1.find(input[i]) == string::npos){ //if the input is not found in SET1, then delete it
				output.push_back(input[i]);
			}
		}
	}

	//-c
	if(comp == 1 && del == 0){
	//	map<char,char>::iterator itr2;
		for(int i=0; i<input.length(); i++){
  		if(s1.find(input[i]) != string::npos){ //if input is  found in SET1,
  			output.push_back(s1[i]); //exists
			}
			else {
				output.push_back(s2.back()); //does not exist
			}
		}
	}

	//-cd
	if(comp == 1 && del == 1){
		for(int i = 0; i < input.length(); i++){
			char temp  = input[i];
			if(s1.find(temp) != string::npos){ //if the input is  found in SET1, then delete it
				output.push_back(input[i]);
			}
		}
	}

	cout<<output;

	return 0;
}
