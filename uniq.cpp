#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
using std::string;
#include <iostream>
using namespace std;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of uniq.  Supported options:\n\n"
"   -c,--count         prefix lines by their counts.\n"
"   -d,--repeated      only print duplicate lines.\n"
"   -u,--unique        only print lines that are unique.\n"
"   --help             show this message and exit.\n";

int main(int argc, char *argv[]) {
	// define long options
	static int showcount=0, dupsonly=0, uniqonly=0;
	static struct option long_opts[] = {
		{"count",         no_argument, 0, 'c'},
		{"repeated",      no_argument, 0, 'd'},
		{"unique",        no_argument, 0, 'u'},
		{"help",          no_argument, 0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "cduh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				showcount = 1;
				break;
			case 'd':
				dupsonly = 1;
				break;
			case 'u':
				uniqonly = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	if (showcount == 1 || (showcount == 0 && dupsonly == 0 && uniqonly == 0)) {
		if (uniqonly == 1 && dupsonly == 0) {
			string s2;
			string s1 = "?";
			size_t count = 0;
		while (getline(cin, s2)) {
			if (s1 == "?")
				s1 = s2;
				if (s2 == s1)
				count++;
				if (s2 != s1) {
					if (count == 1){
						printf ("%7lu ",count);
						cout << "" << s1 << endl;
					}
					count = 0;
					s1 = s2;
					++count;
				}
			}
			if (count == 1) {
				printf ("%7lu ",count);
				cout << "" << s1 << endl;
			}
		}
		else if (dupsonly == 1 && uniqonly == 0) {
			string s2;
			string s1 = "?";
			size_t count = 0;
				while (getline(cin, s2)) {
					if (s1 == "?")
						s1 = s2;
					if (s2 != s1){
						if (count > 1) {
							printf ("%7lu ",count);
							cout << "" << s1 << endl;
							count = 0;
						}
						s1 = s2;
						count = 0;
					}
				++count;
				s1 = s2;
				}
				if (count > 1) {
					printf ("%7lu ",count);
					cout << "" << s1 << endl;
				}
			}
			else if ((dupsonly == 0 && uniqonly == 0)||(showcount == 0 && dupsonly == 0 && uniqonly == 0)) {
				string s2;
				string s1 = "?";
				size_t count = 0;
				while (getline(cin, s2)) {
					if (s1 == "?")
						s1 = s2;
					if (s2 == s1)
						++count;
						else {
							if (showcount == 1) {
								printf ("%7lu ",count);
								cout << "" << s1 << endl;
								}
							if ((showcount == 0 && dupsonly == 0 && uniqonly == 0)){
								cout << "" << s1 << endl;
							}
							count = 0;
							s1 = s2; //update
							++count;
						}
				}
					if (count >= 1) {
						if (showcount == 0 && dupsonly == 0 && uniqonly == 0)
							cout << "" << s1 << endl;
						if (showcount == 1) {
							printf ("%7lu ",count);
							cout << "" << s1 << endl;
						}
					}
				}
}

	if (dupsonly == 1 || uniqonly == 1) {
		if (dupsonly == 1 && uniqonly == 0) {
		string s2;
		string s1 = "?";
		size_t count = 0;
		while (getline(cin, s2)) {
			if (s1 == "?")
				s1 = s2;
			if (s2 != s1){
				if (count > 1) {
					cout << s1 << endl;
					count = 0;
				}
				s1 = s2;
				count = 0;
			}
		++count;
		s1 = s2;
		}
	  if (count > 1)
			cout << s1 << endl;
	}
	else if (dupsonly == 1 && uniqonly == 1)
		printf("%");
	else if (uniqonly == 1) {
		string s2;
		string s1 = "?";
		size_t count = 0;
		while (getline(cin, s2)) {
			if (s1 == "?")
				s1 = s2;
			if (s2 == s1)
				count++;
			if (s2 != s1) {
				if (count == 1)
					cout << s1 << endl;
				count = 0;
				s1 = s2;
				++count;
		}
	}
		if (count == 1)
			cout << s1 << endl; // check the last
	}
}

	return 0;
}
