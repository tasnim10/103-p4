/*Sadia Moumy
 * CSc103 Project 3: unix utilities
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *Classmates
 *CCNY tutors
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours: everyday atleast 2 hours from the day this was posted on piazza
*/
#include <string>
using std::string;
#include <set>
using std::set;
#include <getopt.h> // to parse long arguments.
#include <cstdio> // printf
#include <iostream>
using namespace std;

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of wc.  Supported options:\n\n"
"   -c,--bytes            print byte count.\n"
"   -l,--lines            print line count.\n"
"   -w,--words            print word count.\n"
"   -L,--max-line-length  print length of longest line.\n"
"   -u,--uwords           print unique word count.\n"
"   --help          show this message and exit.\n";

 enum STATE {w,nws};
 size_t Bytes(size_t &bytes)
 {
	bytes++;
	return bytes;
 }
 size_t Lines(size_t &lines,const char ch)
 {
	 if (ch == '\n')
		 lines++;
	 return lines;
 }
 size_t Words(size_t &words, STATE &state, const char ch)
 {
    if (state == nws){
		  if (ch == ' ' || ch == '\t' || ch == '\n'){
				words++;
			}
		}
		return words;
	}
	set<string> uniqueWord (set<string> uniqueWords, string &str, STATE &state, const char ch)
	{
		if (state == nws){
			if(ch == ' ' || ch == '\t' || ch == '\n'){
				uniqueWords.insert(str);
				str.clear();
				state = w;
			}
			else
				str+=ch;
		}
		else {
			if (ch != ' ' && ch !='\t' && ch != '\n'){
				str+=ch;
				state = nws;
			}
		}
		return uniqueWords;
	}
	size_t longLength(size_t &maxLength, size_t &length, const char ch){
		if (ch == '\n'){
			if (length > maxLength){
				maxLength = length;
			}
			length = 0;
		}
		else if (ch == '\t')
			length += 8-length%8;
		else
			length++;
		return maxLength;
	}

int main(int argc, char *argv[])
{
	// define long options
	static int charonly=0, linesonly=0, wordsonly=0, uwordsonly=0, longonly=0;
	static struct option long_opts[] = {
		{"bytes",           no_argument,   0, 'c'},
		{"lines",           no_argument,   0, 'l'},
		{"words",           no_argument,   0, 'w'},
		{"uwords",          no_argument,   0, 'u'},
		{"max-line-length", no_argument,   0, 'L'},
		{"help",            no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "clwuLh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'c':
				charonly = 1;
				break;
			case 'l':
				linesonly = 1;
				break;
			case 'w':
				wordsonly = 1;
				break;
			case 'u':
				uwordsonly = 1;
				break;
			case 'L':
				longonly = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	STATE state;
	char ch;
	size_t bytes = 0, lines = 0, words = 0, length = 0, maxLength = 0;
	string s;
	fread(&ch, 1, 1, stdin);
	if (ch == ' '){
		state = w;
		bytes++;
		s += ch;
		length++;
	}
	else if ( ch== '\t'){
		state = w;
		bytes++;
		length += 8 - length%8;
	}
	else if (ch == '\n') {
		state = w;
		bytes++;
		lines++;
	}
	else{
		state = nws;
		bytes++;
		s += ch;
		length++;
	}
	set <string> uniqueWords;
	while (fread(&ch, 1, 1, stdin)){
		Bytes(bytes);
		Lines(lines, ch);
		Words(words, state, ch);
		uniqueWord(uniqueWords, s, state, ch);
		longLength(maxLength, length, ch);
	}

	if (feof(stdin)){
		if (state == nws){
			uniqueWords.insert(s);
			words++;
		}
	}

	if (linesonly ==1)
		cout << "\t" << lines;
	if (wordsonly ==1)
		cout << "\t" << words;
	if (charonly ==1)
		cout << "\t" << bytes;
	if (uwordsonly ==1){
		size_t total = 0;
		for (set<string>::iterator i = uniqueWords.begin(); i != uniqueWords.end(); i++){
			total++;
		}
		cout << "\t" << total;
	}
	if (longonly == 1)
		cout << "\t" << maxLength;
	if (linesonly != 1 && wordsonly != 1 && charonly != 1 && uwordsonly != 1 && longonly != 1){
		cout << "\t" << lines << "\t" << words << "\t" << bytes;
	}
	cout << "\n";
	return 0;
}
