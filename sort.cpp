#include <cstdio>
#include <getopt.h> // to parse long arguments.
#include <string>
using std::string;
#include <iostream>
using namespace std;
#include <set>
using std::set;
using std::multiset;
#include <map>
using std::map;
#include <strings.h>

static const char* usage =
"Usage: %s [OPTIONS]...\n"
"Limited clone of sort.  Supported options:\n\n"
"   -r,--reverse        Sort descending.\n"
"   -f,--ignore-case    Ignore case.\n"
"   -u,--unique         Don't output duplicate lines.\n"
"   --help              Show this message and exit.\n";

/* NOTE: If you want to be lazy and use sets / multisets instead of
 * linked lists for this, then the following might be helpful: */
struct casefn {
	bool operator()(const string& s1, const string& s2) {
		return (strcasecmp(s1.c_str(),s2.c_str()) < 0);
	}
};
/* How does this help?  Well, if you declare
 *    set<string,igncaseComp> S;
 * then you get a set S which does its sorting in a
 * case-insensitive way! */

int main(int argc, char *argv[]) {
	// define long options
	static int descending=0, ignorecase=0, unique=0;
	static struct option long_opts[] = {
		{"reverse",       no_argument,   0, 'r'},
		{"ignore-case",   no_argument,   0, 'f'},
		{"unique",        no_argument,   0, 'u'},
		{"help",          no_argument,   0, 'h'},
		{0,0,0,0}
	};
	// process options:
	char c;
	int opt_index = 0;
	while ((c = getopt_long(argc, argv, "rfuh", long_opts, &opt_index)) != -1) {
		switch (c) {
			case 'r':
				descending = 1;
				break;
			case 'f':
				ignorecase = 1;
				break;
			case 'u':
				unique = 1;
				break;
			case 'h':
				printf(usage,argv[0]);
				return 0;
			case '?':
				printf(usage,argv[0]);
				return 1;
		}
	}

	if (ignorecase == 0) {
	map <string, int> m;
	string s;
	while (getline(cin, s))
		m[s]++;

	if (unique == 0 && descending == 0 && ignorecase == 0)
	{
		for (map<string,int>::iterator i = m.begin(); i != m.end(); i++) {
			for (int j = 0; j < (*i).second; j++)
				cout << (*i).first << endl;
		}
	}

	else if (unique == 1 && descending == 0 && ignorecase == 0)
	{
		for (map<string,int>::iterator i = m.begin(); i != m.end(); i++)
		cout << (*i).first << endl; // give the key
	}

	else if (unique == 1 && descending == 1 && ignorecase == 0)
	{
		for (map<string,int>::reverse_iterator i = m.rbegin();  i != m.rend(); i++)
		cout << (*i).first << endl; // give the key
	}

	else if (unique == 0 && descending == 1 && ignorecase == 0)
	{
		for (map<string,int>::reverse_iterator i = m.rbegin();  i != m.rend(); i++) {
			for (int j = 0; j < (*i).second; j++)
				cout << (*i).first << endl;
		}
	}
}

if (ignorecase == 1) {
	map<string, int, casefn> m;
	string s;
	while (getline(cin, s))
			m[s]++;
	if (unique == 0 && descending == 0) {
		for (map<string, int, casefn>::iterator i=m.begin(); i != m.end(); i++) {
			for (int j = 0; j < (*i).second; j++)
				cout << (*i).first << endl;
		}
	}
	else if (unique == 1 && descending == 0) {
		for (map<string, int, casefn>::iterator i=m.begin(); i != m.end(); i++)
			cout << (*i).first << endl;
	}
	else if (unique == 0 && descending == 1) {
		for (map<string, int, casefn>::reverse_iterator i=m.rbegin(); i != m.rend(); i++) {
			for (int j = 0; j < (*i).second; j++)
				cout << (*i).first << endl;
			}
		}
	else if (unique == 1 && descending == 1) {
		for (map<string, int, casefn>::reverse_iterator i=m.rbegin(); i != m.rend(); i++)
			cout << (*i).first << endl;
	}
}

	return 0;
}
